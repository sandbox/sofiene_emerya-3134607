<?php

namespace Drupal\commerce_smt\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the Off-site Redirect payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "smt",
 *   label = "SMT (Off-site redirect)",
 *   display_label = "Service Monetique Tunisien",
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_smt\PluginForm\SmtForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "visa", "mastercard",
 *   },
 *   requires_billing_information = FALSE,
 * )
 */
class Smt extends OffsitePaymentGatewayBase {

  const PROD_URL = 'https://www.smt-sps.com.tn/clicktopay/getpurchase.aspx';

  const TEST_URL = 'https://clictopay.monetiquetunisie.com/clicktopay/getpurchase.aspx';

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
        'merchand_id_3ds' => '',
        'merchand_key' => '',
        'decimal' => 0,
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['merchand_id_3ds'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchand id 3ds'),
      '#default_value' => $this->configuration['merchand_id_3ds'],
      '#required' => TRUE,
    ];
    $form['merchand_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchand key'),
      '#default_value' => $this->configuration['merchand_key'],
      '#required' => TRUE,
    ];
    $form['decimal'] = [
      '#type' => 'number',
      '#title' => $this->t('Decimal'),
      '#default_value' => $this->configuration['decimal'],
      '#min' => 0,
      '#max' => 3,
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['merchand_id_3ds'] = $values['merchand_id_3ds'];
      $this->configuration['merchand_key'] = $values['merchand_key'];
      $this->configuration['decimal'] = $values['decimal'];
    }
  }
}