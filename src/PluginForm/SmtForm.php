<?php

namespace Drupal\commerce_smt\PluginForm;

use Drupal\commerce_payment\Exception\HardDeclineException;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\commerce_smt\Plugin\Commerce\PaymentGateway\Smt;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Class SmtForm
 *
 * @package Drupal\commerce_smt\PluginForm
 */
class SmtForm extends BasePaymentOffsiteForm {

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\commerce\Response\NeedsRedirectException
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    $payment->save();
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $payment_gateway_configuration = $payment_gateway_plugin->getConfiguration();
    $orderId = $payment->getOrderId();
    $order = $payment->getOrder();
    $merchand_id = $payment_gateway_configuration['merchand_id_3ds'];
    $merchand_key = $payment_gateway_configuration['merchand_key'];
    $price = number_format($payment->getAmount()
      ->getNumber(), $payment_gateway_configuration['decimal']);
    $time = date('dmYHis');
    $sessionId = $orderId;

    if (\Drupal::requestStack()
      ->getCurrentRequest()->query->get('PAYMENT_REFERENCE', FALSE)) {
      $VERIFICATION_CODE = \Drupal::requestStack()
        ->getCurrentRequest()->query->get('VERIFICATION_CODE', FALSE);
      $code = $order->getData('VERIFICATION_CODE');
      if ($VERIFICATION_CODE == $code) {
        if ($ACKNOLEDGEMENT_URL = \Drupal::requestStack()
          ->getCurrentRequest()->query->get('ACKNOLEDGEMENT_URL', FALSE)) {
          if ($this->verifyAcknoledgementUrl($ACKNOLEDGEMENT_URL)) {

          }
          else{
            throw new HardDeclineException();
          }
        }
      }
      else {
        throw new HardDeclineException();
      }
    }
    $data['ACTION'] = 'PAYMENT';
    $data['DATE_TIME'] = $time;
    $data['CONFIRMATION_URL'] = Url::fromRoute('commerce_checkout.form', [
      'commerce_order' => $orderId,
      'step' => 'payment',
    ], ['absolute' => TRUE])->toString();
    $data['CANCELLATION_URL'] = Url::fromRoute('commerce_checkout.form', [
      'commerce_order' => $orderId,
      'step' => 'payment',
    ], ['absolute' => TRUE])->toString();
    $data['PAYMENT_REFERENCE'] = $orderId;
    $data['AMOUNT'] = $price;
    $data['CURRENCY'] = $payment->getAmount()->getCurrencyCode();
    $data['MERCHANT_ID'] = $merchand_id;
    $data['SESSION_ID'] = $sessionId;
    $data['LANGUAGE'] = \Drupal::languageManager()
      ->getCurrentLanguage()
      ->getId();
    $data['CLIENT_EMAIL'] = $payment->getOrder()
      ->getEmail() ? $payment->getOrder()->getEmail() : $payment->getOrder()
      ->getCustomer()
      ->getEmail();
    $md5 = md5($orderId . $price . $merchand_id . $sessionId . $time . $merchand_key);
    $data['VERIFICATION_CODE'] = $md5;

    $order->setData('VERIFICATION_CODE', $md5)->save();
    if ($payment_gateway_configuration['mode'] == 'live') {
      $redirect_url = Smt::PROD_URL;
    }
    else {
      $redirect_url = Smt::TEST_URL;
    }
    $form = $this->buildRedirectForm($form, $form_state, $redirect_url, $data, self::REDIRECT_POST);
    return $form;
  }

  /**
   * @param $ACKNOLEDGEMENT_URL
   *
   * @return bool
   */
  protected function verifyAcknoledgementUrl($ACKNOLEDGEMENT_URL) {
    $arrContextOptions = [
      "ssl" => [
        "allow_self_signed" => TRUE,
        "verify_peer" => FALSE,
      ],
    ];
    $return = file_get_contents($ACKNOLEDGEMENT_URL, FALSE, stream_context_create($arrContextOptions));
    return strtoupper($return) == 'VERIFIED';
  }
}