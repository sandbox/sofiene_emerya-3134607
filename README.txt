CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Ce projet intègre le systeme de paiement Tunisien dans les systèmes de paiement
et de paiement Drupal Commerce. Il prend actuellement en charge le paiement hors
site via le Service Monetique Tunisien

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/commerce_smt

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/commerce_smt


REQUIREMENTS
------------

 * Drupal Commerce (https://www.drupal.org/project/commerce):
   Drupal Commerce is used to build eCommerce websites and applications of all
   sizes. At its core it is lean and mean, enforcing strict development standards
   and leveraging the greatest features of Drupal 7 and major modules like Views
   and Rules for maximum flexibility.


INSTALLATION
------------

 * Use `composer require drupal/commerce_smt` to install.


CONFIGURATION
-------------

To use the Tunisian payment service, you will need to add the Tunisian gateway payment
from the /admin/commerce/config/payment-gateways/add page.


MAINTAINERS
-----------

Current maintainers:
 * sofiene chaari (sofiene.chaari) - https://www.drupal.org/u/sofienechaari